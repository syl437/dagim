import {Component, OnInit} from '@angular/core';
import {AlertController, NavController , MenuController} from 'ionic-angular';
import {AboutPage} from "../about/about";
import {ShopPage} from "../shop/shop";
import {SendToServerService} from "../../services/send_to_server";
import {ContactPage} from "../contact/contact";
import {SinglePage} from "../single/single";
import {CategoriesPage} from "../categories/categories";
import {BasketService} from "../../services/basket.service";
import {BasketPage} from "../basket/basket";
import {PopupService} from "../../services/popups";
import {UserService} from "../../services/user";
import {LangChangeEvent, TranslateService} from 'ng2-translate';
import {Config} from "../../services/config";


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {
    
    public ServerHost;
    public BasketPrice: any = 0;
    public emptyBasketText;
    public emptyProductsText;
    public Categories:any[] = [];
    public MainCategories:any[] = [];

    constructor(public navCtrl: NavController, public Server: SendToServerService,public menu:MenuController, public BasketService: BasketService, public alertCtrl: AlertController, public Popup: PopupService, public User: UserService, private translate: TranslateService, public Settings: Config) {
        this.menu.enable(true);
        this.ServerHost = this.Settings.ServerHost;
        
        translate.setDefaultLang('he');
        translate.use('he');

        translate.onLangChange.subscribe((event: LangChangeEvent) => {

            this.emptyBasketText = translate.instant("emptybaskettext");
            this.emptyProductsText = translate.instant("addproductstext");
        });
        //window.localStorage.basket = '';


        if(window.localStorage.basket)
        {
            this.BasketService.basket =  JSON.parse(window.localStorage.basket);
            //console.log("Bs : " + JSON.parse(window.localStorage.basket))
        }

        this.Server.GetMainCategories('GetMainCategories').then((data: any) => {
            this.MainCategories = data;
            console.log("MainCategories : " ,data )
        });
    }

    ngOnInit() {
        this.Server.GetAllProducts('getProducts').then((data: any) => {
            console.log("Weights : ", data)
        });
        this.Server.GetAbout('GetAbout').then((data: any) => {
            console.log("About : ", data)
        });
        this.CalculateBasket();
        this.Server.GetCategories('GetCategories').then((data: any) => {
            this.Categories = data
        });
        
        //window.localStorage.userid = '';

    }
    
    imageClick(i,j)
    {
        let Product = this.MainCategories[i].cat_products[j];
        this.navCtrl.push(SinglePage, {product:Product,type:1});
        console.log(Product)
    }
    
    changeLanguage(lang) {
        this.translate.use(lang);
        this.Settings.updateLanguage(lang);
    }

    onGoToAboutPage() {
        this.navCtrl.push(AboutPage);
    }

    goToBasket() {
        if (this.BasketService.basket.length > 0)
            this.navCtrl.push(BasketPage);
        else
            this.Popup.presentAlert(this.emptyBasketText, this.emptyProductsText, '');
    }

    ionViewWillEnter() {
        console.log("ionViewWillEnter")
        this.CalculateBasket();
    }

    onGoToShopPage() {
        this.navCtrl.push(CategoriesPage,{id:"5"});

    }

    onGoToContactPage() {
        this.navCtrl.push(ContactPage);
    }

    CalculateBasket() {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice", this.BasketPrice)
    }


}














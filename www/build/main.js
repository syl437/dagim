webpackJsonp([11],{

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams, Server, domSanitizer, BasketService, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.domSanitizer = domSanitizer;
        this.BasketService = BasketService;
        this.Settings = Settings;
        this.ServerUrl = "http://www.tapper.co.il/dagim/php/";
        this.BasketPrice = 0;
        this.defaultLangage = '';
        this.AboutObject = this.Server.AboutArray[0];
        this.AboutContent = this.AboutObject.content;
        this.AboutContentEnglish = this.AboutObject.content_english;
        this.MainImg = this.ServerUrl + '' + this.AboutObject.image;
        this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.AboutObject.video);
        console.log("AboutArray : ", this.MainImg);
        this.defaultLangage = this.Settings.defaultLanguage;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
        this.CalculateBasket();
    };
    AboutPage.prototype.ngOnInit = function () {
        this.CalculateBasket();
    };
    AboutPage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice About", this.BasketPrice);
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\about\about.html"*/'    <ion-header>\n\n        <header></header>\n\n    </ion-header>\n\n\n\n\n\n\n\n\n\n    <ion-content>\n\n        <div class="AboutTitle">{{\'aboutustitle\' | translate}}</div>\n\n        <div *ngIf="defaultLangage == \'he\'" class="AboutDescription" [innerHTML]="AboutContent"></div>\n\n        <div *ngIf="defaultLangage == \'en\'" class="AboutDescription" [innerHTML]="AboutContentEnglish"></div>\n\n\n\n        <img src="{{MainImg}}" style="width: 100%" />\n\n\n\n        <div>\n\n            <iframe width="100%" height="200" [src]="videoUrl" frameborder="0" allowfullscreen></iframe>\n\n        </div>\n\n\n\n    </ion-content>\n\n\n\n    <ion-footer class="FooterClass">\n\n        <footer></footer>\n\n    </ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\about\about.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_basket_service__["a" /* BasketService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */]) === "function" && _f || Object])
], AboutPage);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=about.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PopupPage = (function () {
    function PopupPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.img_url = this.navParams.get('url');
    }
    PopupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PopupPage');
    };
    PopupPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return PopupPage;
}());
PopupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-popup',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\popup\popup.html"*/'<ion-content>\n\n\n\n  <ion-row>\n\n    <ion-col text-right padding-right>\n\n      <ion-icon name="close" style="font-size: 30px;" (click)="closeModal()"></ion-icon>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  <div class="div90" text-center>\n\n    <img src="{{img_url}}" class="w100" />\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\popup\popup.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]) === "function" && _c || Object])
], PopupPage);

var _a, _b, _c;
//# sourceMappingURL=popup.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ForgotPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ForgotPage = (function () {
    function ForgotPage(navCtrl, navParams, alertCtrl, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Forgot = {
            "mail": ""
        };
        translate.setDefaultLang('he');
        translate.onLangChange.subscribe(function (event) {
        });
    }
    ForgotPage.prototype.sendPassword = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Forgot.mail == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("inputmailtext"), '');
        else if (!this.emailregex.test(this.Forgot.mail)) {
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"), '');
            this.Forgot.mail = '';
        }
        else {
            this.serverResponse = this.Server.ForgotPassword("ForgotPassword", this.Forgot).then(function (data) {
                console.log("forgot response: ", data);
                if (data[0].status == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("maildoesntexiststitletext"), _this.translate.instant("maildoesntexiststext"), '');
                    _this.Forgot.mail = '';
                }
                else {
                    _this.Popup.presentAlert(_this.translate.instant("passwordsenttitletext"), _this.translate.instant("passwordsenttext"), '');
                    _this.Forgot.mail = '';
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
                }
            });
        }
    };
    ForgotPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPage');
    };
    return ForgotPage;
}());
ForgotPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-forgot',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\forgot\forgot.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>{{\'forgotpasstext\' | translate}}</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Forgot.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <div style="width: 100%; margin-top: 2px;" align="center" (click)="sendPassword()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\forgot\forgot.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */]) === "function" && _f || Object])
], ForgotPage);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forgot_forgot__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, alertCtrl, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Login = {
            "mail": "",
            "password": ""
        };
        translate.setDefaultLang('he');
        translate.onLangChange.subscribe(function (event) {
        });
    }
    LoginPage.prototype.loginUser = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Login.mail == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("inputmailtext"), '');
        else if (!this.emailregex.test(this.Login.mail)) {
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"), '');
            this.Login.mail = '';
        }
        else if (this.Login.password == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("missingpasswordtext"), '');
        else {
            this.serverResponse = this.Server.LoginUser("UserLogin", this.Login).then(function (data) {
                console.log("login response: ", data);
                if (data.length == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("badlogintitle"), _this.translate.instant("bademailorpasswordtext"), '');
                    _this.Login.password = '';
                }
                else {
                    window.localStorage.userid = data[0].id;
                    window.localStorage.name = data[0].name;
                    _this.Login.mail = '';
                    _this.Login.password = '';
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__basket_basket__["a" /* BasketPage */]);
                }
            });
        }
    };
    LoginPage.prototype.goRegisterPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.goForgotPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__forgot_forgot__["a" /* ForgotPage */]);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\login\login.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>{{\'logintitletext\' | translate}}</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Login.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'passwordtext\' | translate}}</ion-label>\n\n      <ion-input type="password" [(ngModel)]="Login.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="loginUser()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n    <div style="width: 100%; margin-top: 2px;" align="center" (click)="goRegisterPage()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'registertitletext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n    <div (click)="goForgotPage()">\n\n      <p>{{\'forgotpasstext\' | translate}}</p>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_popups__["a" /* PopupService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */]) === "function" && _f || Object])
], LoginPage);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactPage = (function () {
    function ContactPage(navCtrl, navParams, alertCtrl, Settings, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Settings = Settings;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Contact = {
            'name': '',
            'mail': '',
            'details': '',
            'phone': '',
        };
        translate.onLangChange.subscribe(function (event) {
        });
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage.prototype.SendForm = function () {
        if (this.Contact.name.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredFullnameText"), 0);
        else if (this.Contact.mail.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), 0);
        else if (this.Contact.phone.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredPhonetext"), 0);
        else if (this.Contact.details.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredDescText"), 0);
        else {
            this.Settings.ContactDetails = this.Contact;
            this.Server.sendContactDetails('getContact');
            this.Popup.presentAlert(this.translate.instant("sentOkTitleText"), this.translate.instant("SendOktextText"), 1, this.RedirectFun());
        }
    };
    ContactPage.prototype.RedirectFun = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\contact\contact.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>{{\'contacttitletext\' | translate}}</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label floating>{{\'fullnametext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Contact.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'phonetext\' | translate}}</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Contact.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Contact.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'desctext\' | translate}}</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Contact.details" name="details" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="SendForm()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\contact\contact.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_popups__["a" /* PopupService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["c" /* TranslateService */]) === "function" && _g || Object])
], ContactPage);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ShopPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ShopPage = (function () {
    function ShopPage(navCtrl, navParams, Server, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.Settings = Settings;
        this.defaultLangage = '';
        // this.defaultLangage = this.Settings.defaultLanguage;
        this.ServerHost = this.Settings.ServerHost;
        this.SubCatId = this.navParams.get('subcat');
        this.Id = this.navParams.get('id');
        this.products = this.Server.SubCategories[this.SubCatId].cat_products[this.Id];
        console.log("products:", this.products);
    }
    ShopPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShopPage');
    };
    ShopPage.prototype.ngOnInit = function () {
    };
    ShopPage.prototype.goToSinglePage = function (ProductId) {
        //this.navCtrl.push(SinglePage, {CategoryId: this.CategoryId,SubCatId: this.SubCatId,ProductId: ProductId})
    };
    return ShopPage;
}());
ShopPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-shop',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\shop\shop.html"*/'\n\n<ion-header>\n\n    <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n      <div class="Product" *ngFor="let product of products let i = index" (click)="goToSinglePage(i)">\n\n        <div class="ProductRight">\n\n          <img src="{{ServerHost+product.image}}" class="w100"  />\n\n        </div>\n\n\n\n        <div class="ProductCenter">\n\n            <span *ngIf="defaultLangage == \'he\'">{{product.title}}</span>\n\n            <span *ngIf="defaultLangage == \'en\'">{{product.title_english}}</span>\n\n              <p>{{product.low_price}}<span> {{\'nistext\' | translate}}  {{\'forkgtext\' | translate}}</span></p>\n\n        </div>\n\n\n\n        <div class="ProductLeft" >\n\n          <img src="images/buy.png" class="w100"  />\n\n        </div>\n\n      </div>\n\n    <div class="Line"></div>\n\n  </div>\n\n</ion-content>\n\n\n\n<ion-footer class="FooterClass">\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\shop\shop.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]) === "function" && _d || Object])
], ShopPage);

var _a, _b, _c, _d;
//# sourceMappingURL=shop.js.map

/***/ }),

/***/ 159:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 159;

/***/ }),

/***/ 202:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		593,
		10
	],
	"../pages/basket/basket.module": [
		597,
		9
	],
	"../pages/categories/categories.module": [
		600,
		8
	],
	"../pages/contact/contact.module": [
		603,
		7
	],
	"../pages/forgot/forgot.module": [
		595,
		6
	],
	"../pages/login/login.module": [
		596,
		5
	],
	"../pages/popup/popup.module": [
		594,
		4
	],
	"../pages/register/register.module": [
		598,
		3
	],
	"../pages/shop/shop.module": [
		602,
		2
	],
	"../pages/single/single.module": [
		601,
		1
	],
	"../pages/subcategory/subcategory.module": [
		599,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 202;

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendToServerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerUrl = "http://tapper.co.il/dagim/laravel/public/api/";
var SendToServerService = (function () {
    function SendToServerService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
    }
    ;
    SendToServerService.prototype.GetAllProducts = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Parray", data), _this.ProductsArray = data;
        }).toPromise();
    };
    SendToServerService.prototype.GetAbout = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.AboutArray = data;
        }).toPromise();
    };
    SendToServerService.prototype.GetCategories = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.MainCategories = data;
            //console.log("Categories : " , data)
        }).toPromise();
    };
    SendToServerService.prototype.GetSubCategoriesById = function (url, id) {
        var _this = this;
        var body = new FormData();
        body.append('id', id);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.SubCategories = data;
            //console.log("Categories : " , data)
        }).toPromise();
    };
    SendToServerService.prototype.GetMainCategories = function (url) {
        var _this = this;
        var body = new FormData();
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.SubCategories = data;
            //console.log("Categories : " , data)
        }).toPromise();
    };
    SendToServerService.prototype.SendBasketToServer = function (url, Basket, sum, pay) {
        var userid = window.localStorage.userid;
        var body = 'userid=' + userid.toString() + '&basket=' + JSON.stringify(Basket) + '&sum=' + sum.toString() + '&pay=' + pay.toString();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) {
            console.log("Baskett : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.sendContactDetails = function (url) {
        var body = 'name=' + this.Settings.ContactDetails['name'] + '&details=' + this.Settings.ContactDetails['details'] + '&mail=' + this.Settings.ContactDetails['mail'] + '&phone=' + this.Settings.ContactDetails['phone'];
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) {
            console.log("Contact : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.LoginUser = function (url, params) {
        var body = 'mail=' + params.mail + '&password=' + params.password;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Login : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.RegisterUser = function (url, params) {
        if (params.newsletter == true)
            this.newsletter = 1;
        else
            this.newsletter = 0;
        var body = 'name=' + params.name + '&mail=' + params.mail + '&password=' + params.password + '&address=' + params.address + '&phone=' + params.phone + '&newsletter=' + this.newsletter;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.ForgotPassword = function (url, params) {
        var body = 'mail=' + params.mail;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Forgot : ", data);
        }).toPromise();
    };
    return SendToServerService;
}());
SendToServerService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]) === "function" && _b || Object])
], SendToServerService);

;
var _a, _b;
//# sourceMappingURL=send_to_server.js.map

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserId = "http://tapper.co.il/647/laravel/public/api/";
var Config = (function () {
    function Config() {
        this.UserId = 3;
        this.ServerHost = "http://tapper.co.il/dagim/php/";
        this.defaultLanguage = 'he';
    }
    ;
    Config.prototype.updateLanguage = function (lang) {
        this.defaultLanguage = lang;
    };
    return Config;
}());
Config = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], Config);

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = (function () {
    function UserService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    ;
    UserService.prototype.logOut = function () {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        //this.navCtrl.push(HomePage);
    };
    return UserService;
}());
UserService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _a || Object])
], UserService);

var _a;
//# sourceMappingURL=user.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubcategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shop_shop__ = __webpack_require__(148);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SubcategoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SubcategoryPage = (function () {
    function SubcategoryPage(navCtrl, navParams, Server, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.Settings = Settings;
        this.ServerHost = this.Settings.ServerHost;
        this.CategoryId = navParams.get('id');
        this.SubCategoryArray = this.Server.ProductsArray[this.CategoryId].subcat;
        console.log("categories: ", this.SubCategoryArray);
    }
    SubcategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SubcategoryPage');
    };
    SubcategoryPage.prototype.goToShopPage = function (index, item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__shop_shop__["a" /* ShopPage */], { cat: this.CategoryId, subcat: index });
    };
    return SubcategoryPage;
}());
SubcategoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-subcategory',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\subcategory\subcategory.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="Product" *ngFor="let item of SubCategoryArray let i=index" (click)="goToShopPage(i,item)">\n\n    <img src="{{ServerHost+item.image}}" style="width:100%;">\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\subcategory\subcategory.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]) === "function" && _d || Object])
], SubcategoryPage);

var _a, _b, _c, _d;
//# sourceMappingURL=subcategory.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(286);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(588);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_about_about__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_single_single__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_register_register__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__ = __webpack_require__(589);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_header_header__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_social_sharing__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_in_app_browser__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_login_login__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_user__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27_ng2_translate__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__directives_directives_module__ = __webpack_require__(591);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__["a" /* ShopPage */], __WEBPACK_IMPORTED_MODULE_12__pages_single_single__["a" /* SinglePage */], __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__["a" /* BasketPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_header_header__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__["a" /* PopupPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__["a" /* ForgotPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__["a" /* SubcategoryPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_29__directives_directives_module__["a" /* DirectivesModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/popup/popup.module#PopupPageModule', name: 'PopupPage', segment: 'popup', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/forgot/forgot.module#ForgotPageModule', name: 'ForgotPage', segment: 'forgot', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/basket/basket.module#BasketPageModule', name: 'BasketPage', segment: 'basket', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/subcategory/subcategory.module#SubcategoryPageModule', name: 'SubcategoryPage', segment: 'subcategory', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/categories/categories.module#CategoriesPageModule', name: 'CategoriesPage', segment: 'categories', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/single/single.module#SinglePageModule', name: 'SinglePage', segment: 'single', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/shop/shop.module#ShopPageModule', name: 'ShopPage', segment: 'shop', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["b" /* TranslateModule */].forRoot({
                provide: __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["a" /* TranslateLoader */],
                useFactory: function (http) { return new __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["d" /* TranslateStaticLoader */](http, '/assets/i18n', '.json'); },
                deps: [__WEBPACK_IMPORTED_MODULE_9__angular_http__["b" /* Http */]]
            })
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["b" /* TranslateModule */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__["a" /* ShopPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_single_single__["a" /* SinglePage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__["a" /* BasketPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_header_header__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__["a" /* PopupPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__["a" /* ForgotPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__["a" /* SubcategoryPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__["a" /* SendToServerService */],
            __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_14__services_basket_service__["a" /* BasketService */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_25__services_popups__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_26__services_user__["a" /* UserService */],
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupService = (function () {
    function PopupService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    ;
    PopupService.prototype.presentAlert = function (Title, Message, PageRedirect, fun) {
        if (fun === void 0) { fun = null; }
        var alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                    text: 'סגור',
                    handler: function () { if (PageRedirect == 1) {
                        fun;
                    } }
                }],
            cssClass: 'alertRtl'
        });
        alert.present();
    };
    return PopupService;
}());
PopupService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _a || Object])
], PopupService);

var _a;
//# sourceMappingURL=popups.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BasketService = (function () {
    function BasketService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.basket = [];
        this.Obj = new Object();
        this.BasketTotalPrice = 0;
        this.productFound = 0;
        this._BasketTotalPrice = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.BasketTotalPrice$ = this._BasketTotalPrice.asObservable();
    }
    ;
    BasketService.prototype.AddProductsProducts = function (productId, Qnt, price, name, nameenglish, image) {
        // console.log("Add : " + )
        // this.productFound = 0;
        // if (this.basket.length > 0)
        // {
        //     for (var i = 0; i < this.basket.length; i++) {
        //         if (this.basket[i].id == productId)
        //         {
        //             this.productFound = 1;
        //             this.basket[i].Qnt += Number(Qnt);
        //             //this.basket[i].Pay +=  Number(Pay);
        //         }
        //     }
        // }
        //
        // if (this.productFound == 0)
        // {
        this.Obj = {
            "id": productId,
            "Qnt": Number(Qnt),
            //"Pay":Number(Pay),
            "price": price,
            "name": name,
            "name_english": nameenglish,
            "image": image
        };
        this.basket.push(this.Obj);
        // }
        this.CalculateBasket();
        window.localStorage.basket = JSON.stringify(this.basket);
        console.log("Basket : ", this.basket);
    };
    BasketService.prototype.CalculateBasket = function () {
        this.BasketTotalPrice = 0;
        for (var i = 0; i < this.basket.length; i++) {
            this.BasketTotalPrice += parseInt(this.basket[i].price) * this.basket[i].Qnt;
            console.log("CalculateBasket", this.BasketTotalPrice);
        }
        this.setPrice(this.BasketTotalPrice);
    };
    BasketService.prototype.setPrice = function (value) {
        this.BasketTotalPrice = value;
        this._BasketTotalPrice.next(value);
    };
    BasketService.prototype.resetTotalPrice = function () {
        this.BasketTotalPrice = 0;
    };
    BasketService.prototype.emptyBasket = function () {
        this.basket = [];
    };
    BasketService.prototype.deleteProduct = function (index) {
        this.basket.splice(index, 1);
        this.CalculateBasket();
    };
    return BasketService;
}());
BasketService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]) === "function" && _b || Object])
], BasketService);

;
var _a, _b;
//# sourceMappingURL=basket.service.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popup_popup__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the BasketPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BasketPage = (function () {
    function BasketPage(navCtrl, zone, navParams, Server, BasketService, modalCtrl, Settings, alertCtrl, Popup, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.Server = Server;
        this.BasketService = BasketService;
        this.modalCtrl = modalCtrl;
        this.Settings = Settings;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.translate = translate;
        this.isAvaileble = false;
        this.TotalPrice = 0;
        this.BasketPrice = 0;
        this.defaultLangage = '';
        this.defaultLangage = this.Settings.defaultLanguage;
        translate.onLangChange.subscribe(function (event) {
        });
        this.BasketService.BasketTotalPrice$.subscribe(function (val) {
            _this.zone.run(function () { _this.BasketPrice = val; });
        });
        this.products = JSON.parse(window.localStorage.basket);
        console.log(JSON.parse(window.localStorage.basket));
    }
    BasketPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BasketPage');
    };
    BasketPage.prototype.ngOnInit = function () {
        this.ServerHost = this.Settings.ServerHost;
        //this.products = this.BasketService.basket;
        this.products = JSON.parse(window.localStorage.basket);
        this.TotalPrice = 0;
        for (var i = 0; i < this.products.length; i++) {
            console.log(Number(this.products[i].Qnt) + " : " + Number(this.products[i].price));
            this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
            console.log("TotalPrice : ", this.TotalPrice);
        }
        this.CalculateBasket();
    };
    BasketPage.prototype.deleteProduct = function (index) {
        this.BasketService.deleteProduct(index);
    };
    BasketPage.prototype.SendOrderToServer = function () {
        var _this = this;
        this.Server.SendBasketToServer('getBasket', this.products, this.TotalPrice, this.Pay).then(function (data) { console.log("Basket : ", data), _this.showPopup(); });
    };
    BasketPage.prototype.checkLoggedIn = function () {
        // if (this.checkBasket() == true)
        // {
        //     if (window.localStorage.userid)
        //     {
        //         if(this.Pay == '' || this.Pay == undefined)
        //             this.Popup.presentAlert(this.translate.instant("selectpaytypetext"),this.translate.instant("selectpayoptiontext"),'')
        //         else {
        //this.SendOrderToServer();
        this.showPopup();
        this.BasketService.resetTotalPrice();
        //         }
        //     }
        //     else
        //         this.navCtrl.push(LoginPage);
        // }
    };
    BasketPage.prototype.checkBasket = function () {
        // if (this.products.length ==  0) {
        //     this.Popup.presentAlert(this.translate.instant("emptybaskettext"), this.translate.instant("addproductstext"),'')
        //     return false;
        // }
        // else
        return true;
    };
    BasketPage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice basket", this.BasketPrice);
    };
    BasketPage.prototype.showPopup = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__popup_popup__["a" /* PopupPage */], { url: this.translate.instant("basketpopupimage") }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { _this.BasketService.emptyBasket(), _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]); });
    };
    return BasketPage;
}());
BasketPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-basket',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\basket\basket.html"*/'\n\n<ion-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="Product" *ngFor="let product of products let i=index">\n\n    <div class="ProductRight">\n\n      <p class="priceDv"> {{product.price}}<span>$</span> &nbsp; </p>\n\n    </div>\n\n\n\n    <div class="ProductCenter">\n\n      <span *ngIf="defaultLangage == \'he\'">{{product.name}}</span>\n\n      <span *ngIf="defaultLangage == \'en\'">{{product.name_english}}</span>\n\n      <p>{{product.Qnt}} Qnt &nbsp; </p>\n\n\n\n    </div>\n\n\n\n\n\n\n\n    <div class="ProductLeft" (click)="deleteProduct(i)">\n\n      <img src="{{ServerHost+product.image}}" class="w100"  />\n\n    </div>\n\n  </div>\n\n\n\n <!-- <ion-list radio-group [(ngModel)]="Pay" class="CB item-borderless" no-lines>\n\n    <ion-item class="CBright" no-lines>\n\n      <ion-label item-right class="CoLabel"> {{\'credittext\' | translate}}</ion-label>\n\n      <ion-radio  item-right value="1" name="1" class="CoLabel" checked></ion-radio>\n\n    </ion-item>\n\n    <ion-item class="CBright" no-lines>\n\n      <ion-label item-right class="CoLabel">{{\'cashtext\' | translate}}</ion-label>\n\n      <ion-radio  item-right value="0" name="0" class="CoLabel" [disabled]="isDisabled"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>-->\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer style="background-color: #f1f1f1 !important; height: 70px; ">\n\n  <div class="inviteFooter" (click)="checkLoggedIn()">\n\n      <div class="inviteFooterRight">\n\n        $ Total : {{TotalPrice}}\n\n      </div>\n\n      <div class="inviteFooterLeft" >\n\n        <img src="{{\'addtobasketimage\' | translate}}" class="w100"  />\n\n      </div>\n\n  </div>\n\n  <!--<footer></footer>-->\n\n</ion-footer>\n\n\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\basket\basket.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_basket_service__["a" /* BasketService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_7__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_popups__["a" /* PopupService */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_8_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ng2_translate__["c" /* TranslateService */]) === "function" && _k || Object])
], BasketPage);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=basket.js.map

/***/ }),

/***/ 588:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]; //BasketPage; //CategoriesPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'עמוד ראשי', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-home-outline' },
            { title: 'מי אנחנו', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-people-outline' },
            { title: 'ערוך פרטים אישיים', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-create-outline' },
        ];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]) === "function" && _a || Object)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\app\app.html"*/'<ion-menu [content]="content" side="right" persistent="true">\n\n    <ion-header>\n\n        <ion-toolbar  style="background-color: #022850 !important;">\n\n            <ion-title style="text-align: right;">תפריט צהלה</ion-title>\n\n        </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n        <ion-list>\n\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">\n\n                <ion-icon class="sideMenuIcon" name="{{p.icon}}"></ion-icon>\n\n                <span class="sideMenuText">{{p.title}}</span>\n\n            </button>\n\n        </ion-list>\n\n    </ion-content>\n\n\n\n</ion-menu>\n\n\n\n\n\n<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _d || Object])
], MyApp);

var _a, _b, _c, _d;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(279);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var FooterComponent = (function () {
    function FooterComponent(socialSharing, iab) {
        this.socialSharing = socialSharing;
        this.iab = iab;
    }
    FooterComponent.prototype.Call = function () {
        window.location.href = 'tel:+97225654543';
    };
    FooterComponent.prototype.Facebook = function () {
        //window.location.href = 'https://www.facebook.com/dagimlabait/?fref=ts';
        this.browser = this.iab.create('https://www.facebook.com/dagimlabait/?fref=ts', '_blank', 'location=yes');
        //browser.executeScript(...);
        //browser.insertCSS(...);
        // browser.close();
    };
    FooterComponent.prototype.Mail = function () {
        window.location.href = 'mailto:Dan2772@gmail.com';
    };
    FooterComponent.prototype.Waze = function () {
        window.location.href = "waze://?q=31.766114,35.215068&navigate=yes";
    };
    FooterComponent.prototype.WhatsApp = function () {
        // Share via WhatsApp
        this.socialSharing.shareViaWhatsApp('גם אני הזמנתי דגים באפליקציית דגים עד הבית להורדה לחצו כאן', '', 'linktoandroidoriphone').then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    return FooterComponent;
}());
FooterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'footer',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\components\footer\footer.html"*/'<!-- Generated template for the FooterComponent component -->\n\n\n\n  <ion-list>\n\n    <ion-icon class="icon"> <img src="images/main_icon_1.png" (click)="WhatsApp()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_2.png" (click)="Facebook()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_3.png" (click)="Call()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_4.png" (click)="Mail()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_5.png" (click)="Waze()"style="width: 100%;"/></ion-icon>\n\n  </ion-list>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\components\footer\footer.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _b || Object])
], FooterComponent);

var _a, _b;
//# sourceMappingURL=footer.js.map

/***/ }),

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popups__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var HeaderComponent = (function () {
    function HeaderComponent(navCtrl, BasketService, zone, alertCtrl, Popup) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.BasketService = BasketService;
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        this.BasketService.BasketTotalPrice$.subscribe(function (val) {
            _this.zone.run(function () { _this.BasketPrice = val; });
        });
    }
    HeaderComponent.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad header');
    };
    HeaderComponent.prototype.goToBasket = function () {
        if (this.BasketService.basket.length > 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_basket_basket__["a" /* BasketPage */]);
        else
            this.Popup.presentAlert('סל קניות ריק', 'יש תחילה להזמין מוצרים', '');
    };
    HeaderComponent.prototype.ngOnInit = function () {
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'header',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\components\header\header.html"*/'<!-- Generated template for the HeaderComponent component -->\n\n\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon style="color: white" name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo"  />\n\n      </ion-title>\n\n    </div>\n\n\n\n    <ion-buttons right>\n\n      <button (click)="goToBasket()">\n\n        {{BasketPrice}}\n\n      </button>\n\n    </ion-buttons>\n\n\n\n\n\n  </ion-toolbar>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\components\header\header.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_basket_service__["a" /* BasketService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */]) === "function" && _e || Object])
], HeaderComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=header.js.map

/***/ }),

/***/ 591:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__background_image_background_image__ = __webpack_require__(592);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DirectivesModule = (function () {
    function DirectivesModule() {
    }
    return DirectivesModule;
}());
DirectivesModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */]],
        imports: [],
        exports: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */]]
    })
], DirectivesModule);

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 592:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackgroundImageDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackgroundImageDirective = (function () {
    function BackgroundImageDirective(el) {
        this.el = el;
    }
    BackgroundImageDirective.prototype.ngOnChanges = function (changes) {
        for (var key in changes) {
            if (key == 'source') {
                if (this.source != null && this.source != undefined) {
                    this.el.nativeElement.style.background = "url('" + this.source + "') no-repeat top left / cover";
                }
            }
        }
    };
    return BackgroundImageDirective;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", String)
], BackgroundImageDirective.prototype, "source", void 0);
BackgroundImageDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
        selector: '[background-image]' // Attribute selector
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]) === "function" && _a || Object])
], BackgroundImageDirective);

var _a;
//# sourceMappingURL=background-image.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_contact__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__single_single__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categories_categories__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_user__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_translate__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_config__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HomePage = (function () {
    function HomePage(navCtrl, Server, menu, BasketService, alertCtrl, Popup, User, translate, Settings) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.Server = Server;
        this.menu = menu;
        this.BasketService = BasketService;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.User = User;
        this.translate = translate;
        this.Settings = Settings;
        this.BasketPrice = 0;
        this.Categories = [];
        this.MainCategories = [];
        this.menu.enable(true);
        this.ServerHost = this.Settings.ServerHost;
        translate.setDefaultLang('he');
        translate.use('he');
        translate.onLangChange.subscribe(function (event) {
            _this.emptyBasketText = translate.instant("emptybaskettext");
            _this.emptyProductsText = translate.instant("addproductstext");
        });
        //window.localStorage.basket = '';
        if (window.localStorage.basket) {
            this.BasketService.basket = JSON.parse(window.localStorage.basket);
            //console.log("Bs : " + JSON.parse(window.localStorage.basket))
        }
        this.Server.GetMainCategories('GetMainCategories').then(function (data) {
            _this.MainCategories = data;
            console.log("MainCategories : ", data);
        });
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.Server.GetAllProducts('getProducts').then(function (data) {
            console.log("Weights : ", data);
        });
        this.Server.GetAbout('GetAbout').then(function (data) {
            console.log("About : ", data);
        });
        this.CalculateBasket();
        this.Server.GetCategories('GetCategories').then(function (data) {
            _this.Categories = data;
        });
        //window.localStorage.userid = '';
    };
    HomePage.prototype.imageClick = function (i, j) {
        var Product = this.MainCategories[i].cat_products[j];
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__single_single__["a" /* SinglePage */], { product: Product, type: 1 });
        console.log(Product);
    };
    HomePage.prototype.changeLanguage = function (lang) {
        this.translate.use(lang);
        this.Settings.updateLanguage(lang);
    };
    HomePage.prototype.onGoToAboutPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */]);
    };
    HomePage.prototype.goToBasket = function () {
        if (this.BasketService.basket.length > 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__basket_basket__["a" /* BasketPage */]);
        else
            this.Popup.presentAlert(this.emptyBasketText, this.emptyProductsText, '');
    };
    HomePage.prototype.ionViewWillEnter = function () {
        console.log("ionViewWillEnter");
        this.CalculateBasket();
    };
    HomePage.prototype.onGoToShopPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__categories_categories__["a" /* CategoriesPage */], { id: "5" });
    };
    HomePage.prototype.onGoToContactPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_contact__["a" /* ContactPage */]);
    };
    HomePage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice", this.BasketPrice);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\home\home.html"*/'<ion-header style="height: 90px;">\n\n  <ion-navbar primary >\n\n\n\n    <ion-buttons right>\n\n        <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n            <ion-icon name="md-menu"></ion-icon>\n\n        </button>\n\n             <!--<button (click)="goToBasket()">-->\n\n        <!--{{BasketPrice}}-->\n\n      <!--</button>-->\n\n    </ion-buttons>\n\n    <ion-buttons left>\n\n      <div style="background-color:#fff;">\n\n          <div><img src="images/logo.png" style="width: 190px; padding: 4px;"></div>\n\n      </div>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n    <ion-scroll class="customMenuScroll" scrollX="true" (click)="onGoToShopPage()">\n\n        <div  *ngFor="let item of Categories" class="video-block1" text-center> {{item.title}} <div class="video-block1" text-center>|</div> </div>\n\n    </ion-scroll>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <div  style="margin-bottom: -200px" >\n\n      <ion-slides>\n\n          <ion-slide>\n\n              <img src="images/home/1.jpg" style="width: 100%" />\n\n          </ion-slide>\n\n          <ion-slide>\n\n              <img src="images/home/2.jpg" style="width: 100%" />\n\n          </ion-slide>\n\n          <ion-slide>\n\n              <img src="images/home/5.jpg" style="width: 100%" />\n\n          </ion-slide>\n\n          <ion-slide>\n\n              <img src="images/home/4.jpg" style="width: 100%" />\n\n          </ion-slide>\n\n      </ion-slides>\n\n\n\n      <div *ngFor="let product of MainCategories let i=index" >\n\n          <div class="homeCatTitle" (click)="onGoToShopPage()">{{product.title}}</div>\n\n          <ion-scroll class="customScroll" scrollX="true">\n\n              <div text-center>\n\n                  <div class="homeProduct video-block" *ngFor="let item of product.cat_products let j=index" (click)="imageClick(i,j)">\n\n                      <img *ngIf="item.image"  [src]="ServerHost+item.image" style="width:100%" />\n\n                      <div>\n\n                          <p class="Title">{{item.title}}</p>\n\n                          <p class="homeTitle">Next  </p>\n\n                          <p class="homeDescription">78 views  </p>\n\n                      </div>\n\n                  </div>\n\n              </div>\n\n          </ion-scroll>\n\n      </div>\n\n\n\n\n\n      <!--\n\n    <button ion-button clear class="Height100 no-padding"><img   (click)="onGoToAboutPage()" src="{{\'home_about\' | translate}}" style="width: 100%;" /></button>\n\n    <button ion-button clear class="Height100 no-padding mt2"><img (click)="onGoToShopPage()" class="mt1"   src="{{\'home_shop\' | translate}}" /></button>\n\n    <button ion-button clear class="Height100 no-padding mt2"><img class="mt1"   src="{{\'home_contact\' | translate}}" (click)="onGoToContactPage()" /></button> -->\n\n  </div>\n\n</ion-content>\n\n<!--\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer> -->'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_basket_service__["a" /* BasketService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_9__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__services_popups__["a" /* PopupService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_10__services_user__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__services_user__["a" /* UserService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_11_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11_ng2_translate__["c" /* TranslateService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_12__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_12__services_config__["a" /* Config */]) === "function" && _j || Object])
], HomePage);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SinglePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__categories_categories__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the SinglePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SinglePage = (function () {
    function SinglePage(navCtrl, navParams, Server, alertCtrl, Settings, BasketService, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.alertCtrl = alertCtrl;
        this.Settings = Settings;
        this.BasketService = BasketService;
        this.Popup = Popup;
        this.translate = translate;
        this.ProductIndex = '';
        this.CategoryId = '';
        this.SubCatId = '';
        this.Product = '';
        this.Qnt = 1;
        this.productId = '';
        this.Price = '';
        this.defaultLangage = '';
        this.Fav = 0;
        this.AC1 = '';
        this.AC2 = '';
        this.answer = 'תשובה לשאלה מספר 1';
        translate.onLangChange.subscribe(function (event) {
        });
        /*    this.ServerHost = this.Settings.ServerHost;
            this.SubCatId = this.navParams.get('subcat');
            this.ProductIndex = this.navParams.get('id');
            this.products = this.Server.SubCategories[this.SubCatId].cat_products[this.Id ];
            console.log ("products:" , this.products)
      */
        this.Type = navParams.get('type');
        this.ServerHost = this.Settings.ServerHost;
        if (this.Type == 0) {
            this.ProductIndex = navParams.get('id');
            this.CategoryId = navParams.get('cat');
            this.SubCatId = navParams.get('subcat');
            this.Product = this.Server.SubCategories[this.SubCatId].cat_products[this.ProductIndex];
        }
        if (this.Type == 1) {
            this.Product = navParams.get('product');
        }
        this.defaultLangage = this.Settings.defaultLanguage;
        this.AC1 = 'closed';
        this.AC2 = 'closed';
        if (this.BasketService.basket.length > 0)
            this.BasketLen = this.BasketService.basket.length;
        console.log("Product : ", this.BasketService.basket);
    }
    SinglePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SinglePage');
    };
    SinglePage.prototype.goback = function () {
        this.navCtrl.pop();
        console.log('Click on button Test Console Log');
    };
    SinglePage.prototype.AddToBasket = function () {
        this.Price = this.Product['high_price'];
        this.BasketService.AddProductsProducts(this.productId, this.Qnt, this.Price, this.Product['title'], this.Product['title_english'], this.Product['image']);
        this.presentConfirm();
    };
    SinglePage.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant("prdaddcattext"),
            message: this.translate.instant("continueshoppingconfirmtext"),
            buttons: [
                {
                    text: this.translate.instant("continueshoppingtext"),
                    role: 'cancel',
                    handler: function () {
                        _this.goToCategoriesPage();
                    }
                },
                {
                    text: this.translate.instant("movebaskettext"),
                    handler: function () {
                        _this.goToBasket();
                    }
                }
            ]
        });
        alert.present();
    };
    SinglePage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.CalculateBasket();
    };
    SinglePage.prototype.goToBasket = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__basket_basket__["a" /* BasketPage */]);
    };
    SinglePage.prototype.goToCategoriesPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__categories_categories__["a" /* CategoriesPage */]);
    };
    SinglePage.prototype.GoToNextPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    SinglePage.prototype.changeFav = function () {
        if (this.Fav == 0)
            this.Fav = 1;
        else
            this.Fav = 0;
    };
    SinglePage.prototype.openItem = function () {
        this.AC1 = this.AC1 === 'closed' ? 'expanded' : 'closed';
    };
    SinglePage.prototype.openItem1 = function () {
        this.AC2 = this.AC2 === 'closed' ? 'expanded' : 'closed';
    };
    return SinglePage;
}());
SinglePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-single',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\single\single.html"*/'<ion-content style="background-color: #f7f7f7">\n\n    <div class="Content">\n\n        <div>\n\n            <img src="{{ServerHost+Product.details_image}}" class="ImageW"/>\n\n            <div class="closeButton" (click)="goback()">\n\n                <ion-icon class="addIcon" name="ios-arrow-back"></ion-icon>\n\n            </div>\n\n            <div class="FavButton" (click)="changeFav()">\n\n                <img *ngIf="Fav == 0" src="images/e_heart1.png" style="width: 100%" />\n\n                <img *ngIf="Fav == 1" src="images/f_heart.png" style="width: 100%" />\n\n            </div>\n\n            <div class="BasketButton">\n\n                <ion-icon class="addIcon" name="ios-basket-outline"></ion-icon>\n\n                <span class="badgeText">{{BasketLen}}</span>\n\n            </div>\n\n        </div>\n\n\n\n\n\n\n\n        <div class="SingleTitle">\n\n           <!-- <div class="SingleTitleLeft" align="center">\n\n                <div style="width:90%">\n\n                    <img src="{{ServerHost+Product.image}}" class="ImageW"/>\n\n                </div>\n\n            </div> -->\n\n            <!--<div *ngIf="defaultLangage == \'he\'" class="SingleTitleLeft">{{Product.title}}</div>-->\n\n            <div class="SingleTitleLeft">{{Product.title_english}}</div>\n\n            <div class="SingleTitleRight">34$</div>\n\n        </div>\n\n        <div class="shipped">\n\n            <img src="images/shipped.png" style="width: 100%" />\n\n        </div>\n\n\n\n\n\n        <ion-list>\n\n            <div>\n\n                <button ion-item (click)="openItem(0)" no-border>\n\n                    <div item-start>\n\n                        <ion-icon class="addIcon" name="ios-add"></ion-icon>\n\n                    </div>\n\n                    <div item-content class="itemTitle">Product Description</div>\n\n                </button>\n\n                <ion-item *ngIf="AC1 == \'expanded\'" no-border>\n\n                    <div item-content [innerHTML]=\'Product.description_english\' class="itemDesc"></div>\n\n                </ion-item>\n\n            </div>\n\n        </ion-list>\n\n\n\n        <ion-list>\n\n            <div>\n\n                <button ion-item (click)="openItem1(0)" no-border>\n\n                    <div item-start>\n\n                        <ion-icon class="addIcon" name="ios-add"></ion-icon>\n\n                    </div>\n\n                    <div item-content class="itemTitle">Reviews</div>\n\n                </button>\n\n                <ion-item *ngIf="AC2 == \'expanded\'" no-border>\n\n                    <div item-content  class="itemDesc">here comes reviews</div>\n\n                </ion-item>\n\n            </div>\n\n        </ion-list>\n\n\n\n\n\n\n\n        <!-- <div *ngIf="defaultLangage == \'he\'" class="singleInfo">{{Product.description}}</div>\n\n         <div *ngIf="defaultLangage == \'en\'" class="singleInfo">{{Product.description_english}}</div>\n\n         <div style="width: 100%" align="center">\n\n             <div class="singlePrice">\n\n                 {{\'priceuntiltext\' | translate}}&nbsp;<span>3</span>&nbsp; {{\'kilotext\' | translate}} &nbsp;<span>{{Product.low_price}}</span>&nbsp;\n\n                 {{\'nistext\' | translate}}&nbsp;&nbsp; &nbsp;| {{\'priceabovetext\' | translate}}&nbsp;<span>3</span>&nbsp;\n\n                 {{\'kilotext\' | translate}} &nbsp;<span>{{Product.high_price}}</span>&nbsp; {{\'nistext\' | translate}}&nbsp;\n\n             </div>\n\n         </div>\n\n\n\n         <div class="singleQnt">\n\n             <div class="singleQntRight">\n\n                 {{\'choosequantext\' | translate}}\n\n             </div>\n\n             <div class="singleQntCenter" no-lines>\n\n\n\n                 <ion-input type="number" [value]="Qnt" name="Qnt" [(ngModel)]="Qnt" class="QntInput"\n\n                            no-lines></ion-input>\n\n             </div>\n\n\n\n\n\n             <div class="singleQntLeft">\n\n                 {{\'kilotext\' | translate}}\n\n             </div>\n\n         </div>-->\n\n    </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer no-border style="background-color: #f7f7f7">\n\n    <table class="footerClass">\n\n        <tr>\n\n            <td (click)="AddToBasket()">\n\n                <img src="images/add_to_bag.png" style="width: 100%" />\n\n            </td>\n\n            <td (click)="goToBasket()">\n\n                <img src="images/go_to_basket.png" style="width: 100%" />\n\n            </td>\n\n        </tr>\n\n    </table>\n\n</ion-footer>\n\n<!--\n\n<ion-footer style="background-color: darkblue !important; height: 130px; ">\n\n    <div class="inviteFooter" (click)="AddToBasket()">\n\n        {{\'ordertext\' | translate}}\n\n    </div>\n\n    <footer></footer>\n\n</ion-footer>-->\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\single\single.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_basket_service__["a" /* BasketService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_popups__["a" /* PopupService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_9_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9_ng2_translate__["c" /* TranslateService */]) === "function" && _h || Object])
], SinglePage);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=single.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basket_basket__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_basket_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popups__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, Settings, alertCtrl, Server, BasketService, modalCtrl, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Settings = Settings;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.BasketService = BasketService;
        this.modalCtrl = modalCtrl;
        this.Popup = Popup;
        this.translate = translate;
        this.isAvaileble = false;
        this.TotalPrice = 0;
        this.Details = {
            'name': '',
            'mail': '',
            'password': '',
            'address': '',
            'phone': '',
            'newsletter': false
        };
        translate.onLangChange.subscribe(function (event) {
        });
        translate.setDefaultLang('he');
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.ngOnInit = function () {
        this.ServerHost = this.Settings.ServerHost;
        this.products = this.BasketService.basket;
        this.TotalPrice = 0;
        for (var i = 0; i < this.products.length; i++) {
            this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
        }
        console.log("cart:", this.products);
    };
    RegisterPage.prototype.doRegister = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Details.name.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredFullnameText"), '');
        else if (this.Details.address.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("missingaddresstext"), '');
        else if (this.Details.mail == "")
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), '');
        else if (!this.emailregex.test(this.Details.mail)) {
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), '');
            this.Details.mail = '';
        }
        else if (this.Details.password == "")
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("missingpasswordtext"), '');
        else {
            this.serverResponse = this.Server.RegisterUser("RegisterUser", this.Details).then(function (data) {
                console.log("register response: ", data);
                if (data[0].status == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("missingFieldsText"), _this.translate.instant("mailalreadyexiststext"), '');
                    _this.Details.mail = '';
                }
                else {
                    window.localStorage.userid = data[0].userid;
                    window.localStorage.name = _this.Details.name;
                    _this.Details.name = '';
                    _this.Details.mail = '';
                    _this.Details.address = '';
                    _this.Details.phone = '';
                    _this.Details.newsletter = false;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__basket_basket__["a" /* BasketPage */]);
                }
            });
        }
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\register\register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n    <ion-item no-lines class="RegTitle">\n\n      <label>{{\'registertitletext\' | translate}}</label>\n\n    </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label floating>{{\'fullnametext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'addresstext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.address" name="address" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'passwordtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'phonetext\' | translate}}</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item style="margin-top: 20px;" no-lines>\n\n      <ion-label>{{\'newslettertext\' | translate}}</ion-label>\n\n      <ion-checkbox color="dark" [(ngModel)]="Details.newsletter" ></ion-checkbox>\n\n    </ion-item>\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="doRegister()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\register\register.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_basket_service__["a" /* BasketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_basket_service__["a" /* BasketService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_6__services_popups__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_popups__["a" /* PopupService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */]) === "function" && _j || Object])
], RegisterPage);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=register.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__single_single__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CategoriesPage = (function () {
    function CategoriesPage(navCtrl, navParams, Server, Settings) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.Settings = Settings;
        this.SubCategories = [];
        this.Products = [];
        this.isAvailble = false;
        this.CurrentSubCat = 0;
        this.CategoryId = 5;
        this.ServerHost = this.Settings.ServerHost;
        this.ProductsArray = this.Server.ProductsArray;
        this.Server.GetSubCategoriesById('GetSubCategoriesById', this.CategoryId).then(function (data) {
            _this.SubCategories = data;
            _this.getProductsArray(_this.CurrentSubCat);
            console.log("SubCategories : ", data);
        });
    }
    CategoriesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriesPage');
    };
    CategoriesPage.prototype.gotToShopPage = function (index) {
        // this.navCtrl.push(SubcategoryPage, {id: index});
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__single_single__["a" /* SinglePage */], { cat: this.CategoryId, subcat: this.CurrentSubCat, id: index, type: 0 });
    };
    CategoriesPage.prototype.getProductsArray = function (id) {
        this.CurrentSubCat = id;
        this.Products = this.SubCategories[id].cat_products;
        this.isAvailble = true;
        console.log("PR : ", this.Products);
    };
    return CategoriesPage;
}());
CategoriesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-categories',template:/*ion-inline-start:"C:\Users\USER\Documents\github\dagim\src\pages\categories\categories.html"*/'<ion-header style="height: 90px">\n\n    <ion-navbar primary>\n\n        <ion-buttons right>\n\n            <button (click)="goToBasket()" *ngIf="isAvailble">\n\n                {{BasketPrice}}\n\n            </button>\n\n        </ion-buttons>\n\n        <ion-buttons left>\n\n            <div style="background-color:#fff;">\n\n                <div><img src="images/logo.png" style="width: 80px; padding: 4px;"></div>\n\n            </div>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n    <ion-scroll class="customMenuScroll" scrollX="true" *ngIf="isAvailble">\n\n        <div  *ngFor="let item of SubCategories let i=index" class="video-block1" text-center (click)="getProductsArray(i)"> {{item.title}} <div class="video-block1" text-center>|</div> </div>\n\n    </ion-scroll>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n    <div class="Content">\n\n        <div class="Product" *ngFor="let item of Products let i=index" (click)="gotToShopPage(i)">\n\n            <div *ngIf="i%2 == 0" class="mainItem" align="center">\n\n                <div style="width: 90%">\n\n                    <div class="mainItemLeft">\n\n                        <img src="{{ServerHost+Products[i].image}}" style="width:100%;">\n\n                        <div class="mainItemTitle">{{item.title}}</div>\n\n                        <hr class="mainHr">\n\n                        <div class="priceDiv">\n\n                            <div class="priceLeft"><span>{{item.high_price}} $</span> </div>\n\n                            <div class="priceRight">More</div>\n\n                        </div>\n\n                    </div>\n\n                    <div class="mainItemRight" *ngIf="i < Products.length-1">\n\n                        <img src="{{ServerHost+Products[i+1].image}}" style="width:100%;">\n\n                        <div class="mainItemTitle">{{item.title}}</div>\n\n                        <hr class="mainHr">\n\n                        <div class="priceDiv">\n\n                            <div class="priceLeft"><span>{{Products[i+1].high_price}} $</span> </div>\n\n                            <div class="priceRight">More</div>\n\n                        </div>\n\n\n\n                    </div>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\dagim\src\pages\categories\categories.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]) === "function" && _d || Object])
], CategoriesPage);

var _a, _b, _c, _d;
//# sourceMappingURL=categories.js.map

/***/ })

},[281]);
//# sourceMappingURL=main.js.map
import {Component, ViewChild} from '@angular/core';
import {Nav,Platform, MenuController } from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {AboutPage} from "../pages/about/about";
import {ShopPage} from "../pages/shop/shop";
import {SinglePage} from "../pages/single/single";
import {BasketPage} from "../pages/basket/basket";
import {RegisterPage} from "../pages/register/register";
import {LoginPage} from "../pages/login/login";
import {ForgotPage} from "../pages/forgot/forgot";
import {CategoriesPage} from "../pages/categories/categories";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage: any = HomePage; //BasketPage; //CategoriesPage;
    pages: Array<{ title: string, component: any , icon:string }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {

        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'עמוד ראשי', component: HomePage, icon: 'ios-home-outline'},
            {title: 'מי אנחנו', component: HomePage, icon: 'ios-people-outline'},
            {title: 'ערוך פרטים אישיים', component: HomePage, icon: 'ios-create-outline'},
            // {title: 'תבנית הערות', component: NotesPage, icon: 'ios-document-outline'},
            // {title: 'התנתק', component: 'LogOut', icon: 'ios-close-circle-outline'}
        ];


        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}


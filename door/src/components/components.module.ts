import { NgModule } from '@angular/core';
import { FooterComponent } from './footer/footer';
import { HeaderComponent } from './header/header';
import { MainHeaderComponent } from './main-header/main-header';
@NgModule({
	declarations: [FooterComponent,
    HeaderComponent,
    MainHeaderComponent],
	imports: [],
	exports: [FooterComponent,
    HeaderComponent,
    MainHeaderComponent]
})
export class ComponentsModule {}

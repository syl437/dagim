import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {Config} from "../../services/config";
import {ShopPage} from "../shop/shop";

/**
 * Generated class for the SubcategoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subcategory',
  templateUrl: 'subcategory.html',
})
export class SubcategoryPage {

    public SubCategoryArray;
    public ServerHost;
    public CategoryId;

    constructor(public navCtrl: NavController, public navParams: NavParams, public Server:SendToServerService, public Settings:Config) {
      this.ServerHost = this.Settings.ServerHost;
        this.CategoryId = navParams.get('id');
      this.SubCategoryArray = this.Server.ProductsArray[this.CategoryId].subcat;
      console.log("categories: ", this.SubCategoryArray);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcategoryPage');
  }

    goToShopPage(index,item)
    {
        this.navCtrl.push(ShopPage, {cat:this.CategoryId,subcat:index});
    }

}
